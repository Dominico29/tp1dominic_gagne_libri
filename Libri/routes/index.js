var express = require('express');
var router = express.Router();
let blog = require("../custom module/blog.js")
let formation = require("../custom module/formation.js")

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Acceuil' });
});
router.get('/blog', function(req, res, next) {
  res.render('blog', { title: 'Blog', blog: blog.allBlogs});
});
router.get('/formation', function(req, res, next) {
  res.render('formation', { title: 'Formation' , formation: formation.allFormations });
});
router.get('/contact', function(req, res, next) {
  res.render('contact', { title: 'Contact' });
});
router.get('/blog/:id' ,function(req, res, next) {
res.render('miniblog' ,{blog:blog.getBlogById(req.params.id)})
})

module.exports = router;
