class blog{
    constructor(id, title, nom, prenom, desc, annee){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom; 
        this.desc = desc; 
        this.annee = annee;
        this.title = title;
    }
}

const datas = [   
    new blog( 0, "Premier blog", "Sam ","maitre ganj","Un beau nom affreux",2012),
    new blog( 1, "Deuxième blog" ,"Boby","Hades","Wood, habitant de la ville saint-bruno, capable de rien, est un programeur raté",2009),
    new blog( 2, "Troisième blog" ,"Karine","Wyskie","La fameuse Bachelorette d'été ",2004), 
    new blog( 3, "Quatrième blog" ,"Charles","Gilbert","Montre des trucs à la caméra",2010),
    new blog( 4, "Cinquième blog","Francois","Mary","Elle n'a pas de noix",2000),
    new blog( 5, "Sixième blog" ,"Bill","Hader","Poueo odfsgkfg sdkfj gn",2089),
    new blog( 6, "Septième blog" ,"Billo","Winslet","sdfksg pdf spfdgsowa",2036),
    new blog( 7, "Huitième blog" ,"Billy","Oedekerk","hate my ex",2035),
    new blog( 8, "Neuvième blog" ,"Billa","Oedekerk","hated my experience",2025),
    new blog( 9, "Dixième blog" ,"Billon","Oedekerk","hating my ex",2065),
];

exports.getBlogById = function(id){
   return datas[id]; 
}

exports.allBlogs = datas;
